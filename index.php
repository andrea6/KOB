<?php
require_once 'vendor/autoload.php';


function get_url($path){

    $base_dir  = __DIR__; // Absolute path to your installation, ex: /var/www/mywebsite
    $doc_root  = preg_replace("!${_SERVER['SCRIPT_NAME']}$!", '', $_SERVER['SCRIPT_FILENAME']); # ex: /var/www
    $base_url  = preg_replace("!^${doc_root}!", '', $base_dir); # ex: '' or '/mywebsite'
    $protocol  = empty($_SERVER['HTTPS']) ? 'http' : 'https';
    $port      = $_SERVER['SERVER_PORT'];
    $disp_port = ($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port";
    $domain    = $_SERVER['SERVER_NAME'];
    $full_url  = "${protocol}://${domain}${disp_port}${base_url}"; # Ex: 'http://example.com', 'https://example.com/mywebsite', etc.

    // Patch Andrea to make it work on localhost
    return $path;
    // Endpatch
    return $full_url."/".$path;
}

function get_url_function($path)
{

    $base_dir = __DIR__; // Absolute path to your installation, ex: /var/www/mywebsite
    $doc_root = preg_replace("!${_SERVER['SCRIPT_NAME']}$!", '', $_SERVER['SCRIPT_FILENAME']); # ex: /var/www
    $base_url = preg_replace("!^${doc_root}!", '', $base_dir); # ex: '' or '/mywebsite'
    $protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';
    $port = $_SERVER['SERVER_PORT'];
    $disp_port = ($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port";
    $domain = $_SERVER['SERVER_NAME'];
    $full_url = "${protocol}://${domain}${disp_port}${base_url}"; # Ex: 'http://example.com', 'https://example.com/mywebsite', etc.


    return $full_url . "/" . $path;
}

$filter = new Twig_SimpleFunction('assets', function ($string) {
    return get_url($string);
});

$url = new Twig_SimpleFunction('url', function ($string) {
    return get_url_function($string);
});

$loader = new Twig_Loader_Filesystem('templates');

$twig = new Twig_Environment($loader);

$twig->addFunction($filter);
$twig->addFunction($url);

if(isset($_GET['r']))
    $tml_name = $_GET['r'];
else
    $tml_name = "index";

if (isset($_GET['tab']))
    $tab_name = $_GET['tab'];
else
    $tab_name = "home";

echo $twig->render($tml_name . '.twig', array('name' => 'Andrea',
    $tml_name => 'active',
    $tab_name => 'open'));

